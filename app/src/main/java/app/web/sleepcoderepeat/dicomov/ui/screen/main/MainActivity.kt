package app.web.sleepcoderepeat.dicomov.ui.screen.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.forEach
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.ui.adapter.MainPagerAdapter
import app.web.sleepcoderepeat.dicomov.ui.fragment.MoviesFragment
import app.web.sleepcoderepeat.dicomov.ui.fragment.TvShowsFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        setupBottomNavigation()
    }

    private fun setupBottomNavigation() {
        view_pager.adapter = MainPagerAdapter(supportFragmentManager).apply {
            addNavigationFragment(MoviesFragment())
            addNavigationFragment(TvShowsFragment())
        }

        bottom_nav.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.action_movies -> view_pager.currentItem = 0
                R.id.action_tv -> view_pager.currentItem = 1
            }
            false
        }

        view_pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                bottom_nav.menu.forEach {
                    it.isChecked = false
                }
                bottom_nav.menu.getItem(position).isChecked = true
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

    }

}
package app.web.sleepcoderepeat.dicomov.ui.screen.detailtv

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.databinding.ActivityDetailMovieBinding
import app.web.sleepcoderepeat.dicomov.databinding.ActivityDetailTvBinding
import app.web.sleepcoderepeat.dicomov.ui.adapter.GenreItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.dialog.VideoDialog
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.DetailMovieViewModel
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.DetailTvViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.obtainViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailTvActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityDetailTvBinding
    private lateinit var dataIntent: Intent
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = ActivityDetailTvBinding.inflate(layoutInflater).apply {
            vm = obtainViewModel(DetailTvViewModel::class.java)
        }
        viewBinding.lifecycleOwner = this
        setContentView(viewBinding.root)

        dataIntent = intent

        setUpObserver()
        setUpRecyclerView()
    }

    private fun setUpObserver() {
        viewBinding.vm?.apply {
            dataIntent.getStringExtra(Constant.TYPE_KEY)?.let {
                getDetail(dataIntent.getIntExtra(Constant.ID_KEY, 0),true)
            }
        }
    }

    private fun setUpRecyclerView() {
        viewBinding.vm?.apply {
            genreList.value?.let {
                viewBinding.rvMovieGenre.adapter = GenreItemAdapter(it)
            }
            message.observe(this@DetailTvActivity,{
                Toast.makeText(this@DetailTvActivity,it, Toast.LENGTH_LONG).show()
            })
        }
    }
}
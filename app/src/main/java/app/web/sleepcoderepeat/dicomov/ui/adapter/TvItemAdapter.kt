package app.web.sleepcoderepeat.dicomov.ui.adapter

import android.app.Activity
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.databinding.TvItemBinding
import app.web.sleepcoderepeat.dicomov.ui.listener.ItemClickListener
import app.web.sleepcoderepeat.dicomov.ui.viewmodel.TvViewModel
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.getGenreMap
import app.web.sleepcoderepeat.dicomov.utils.getSizeByScreenSize
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl

class TvItemAdapter constructor(
    private var tvItemList: MutableList<TvItem>,
    private val mainViewModel: TvViewModel,
    val context: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieItemHolder(
            context,
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.tv_item,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        tvItemList[position].apply {
            (holder as MovieItemHolder).bind(this, object : ItemClickListener {
                override fun onItemClick() {
                    id?.let {
                        mainViewModel.openDetailTv(it)
                    }
                }
            })
        }
    }

    override fun getItemCount(): Int = tvItemList.size

    fun replaceData(movieList: MutableList<TvItem>) {
        setList(movieList)
    }

    private fun setList(movieList: MutableList<TvItem>) {
        this.tvItemList = movieList
        notifyDataSetChanged()
    }

    class MovieItemHolder constructor(
        val context: Activity,
        val binding: TvItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(tvItem: TvItem, mainActionListener: ItemClickListener) {
            val item = tvItem.copy()
            tvItem.poster_path?.let {
                item.poster_path = it.toImageUrl()
            }
            binding.tvImage.clipToOutline = true
            binding.tvItem = item
            val genreList: MutableList<Genre> = arrayListOf()
            val genreMap = getGenreMap()
            tvItem.genre_ids?.apply {
                map { i ->
                    if (!genreMap[i].isNullOrBlank())
                        genreList.add(Genre(i, genreMap[i]))
                }
            }
            val dm = DisplayMetrics()
            context.windowManager.defaultDisplay.getMetrics(dm)
            val sized = dm.getSizeByScreenSize()
            binding.tvItemLayout.apply {
                layoutParams.width = sized
                layoutParams.height = sized + (sized / 2)
            }
            binding.rvTvGenre.adapter = GenreItemAdapter(genreList)
            binding.action = mainActionListener
            binding.executePendingBindings()
        }

    }
}
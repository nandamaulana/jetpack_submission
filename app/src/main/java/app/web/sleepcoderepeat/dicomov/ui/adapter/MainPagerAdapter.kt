package app.web.sleepcoderepeat.dicomov.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class MainPagerAdapter(fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val listNavigation: MutableList<Fragment> = arrayListOf()

    override fun getItem(position: Int): Fragment = listNavigation[position]

    override fun getCount(): Int = listNavigation.size

    fun addNavigationFragment(fragment: Fragment) = listNavigation.add(fragment)

}
package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

data class Network(
    val id: Int,
    val logo_path: String,
    val name: String,
    val origin_country: String
)
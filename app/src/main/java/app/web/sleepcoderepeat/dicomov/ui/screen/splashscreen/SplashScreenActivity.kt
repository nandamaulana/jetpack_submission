package app.web.sleepcoderepeat.dicomov.ui.screen.splashscreen

import android.content.Intent
import android.os.Bundle
import android.view.animation.Animation
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import app.web.sleepcoderepeat.dicomov.R
import app.web.sleepcoderepeat.dicomov.ui.screen.main.MainActivity
import app.web.sleepcoderepeat.dicomov.utils.getAnim
import kotlinx.android.synthetic.main.activity_splash_screen.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var topAnim: Animation
    private lateinit var bottomAnim: Animation


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        initAnimation()
        GlobalScope.launch {
            startAnimation()
            delay(1500L)
            startActivity(
                Intent(this@SplashScreenActivity, MainActivity::class.java),
                ActivityOptionsCompat.makeCustomAnimation(
                    applicationContext,
                    android.R.anim.fade_in, android.R.anim.fade_out
                ).toBundle()
            )
            finish()
        }
    }

    private fun initAnimation() {
        topAnim = getAnim(R.anim.splash_top)
        bottomAnim = getAnim(R.anim.splash_bottom)
    }

    private fun startAnimation() {
        rl_movie_icon.startAnimation(topAnim)
        tv_app_name.startAnimation(bottomAnim)
    }
}
package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvListResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.SingleLiveEvent
import app.web.sleepcoderepeat.dicomov.utils.format
import io.reactivex.disposables.CompositeDisposable
import java.util.*
import kotlin.collections.HashMap

class TvViewModel @ViewModelInject constructor(
    private val movieTvRepository: MovieTvRepository
) : ViewModel() {


    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val tvList = MutableLiveData<MutableList<TvItem>>()
    val message = MutableLiveData<String>()
    val loading = MutableLiveData<Boolean>()
    val page = MutableLiveData<String>()
    val openDetailTv = SingleLiveEvent<Int>()


    init {
        loading.value = false
        page.value = "1"
    }

    fun getListTv() {
        loading.value = true
        movieTvRepository.getDataDiscoverTv(
            HashMap<String?, String?>().apply {
                put(Constant.PAGE_KEY, page.value)
                put(Constant.PRIMARY_RELEASE_DATE_KEY, Calendar.getInstance().time.format())
                put(Constant.SORT_BY_KEY, Constant.POPULARITY_DESC)
            },
            object : MovieTvSource.TvListCallback {
                override fun onSuccess(tvListResponse: TvListResponse) {
                    tvList.value?.clear()
                    tvList.value = tvListResponse.tvListItem
                    loading.value = false
                }

                override fun onNotAvailable() {
                    message.value = NOT_AVAILABLE
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        message.value = msg
                    }
                    loading.value = false
                }
            }
        ).apply {
            if (!isDisposed) {
                disposable.add(this)
            }
        }
    }

    fun openDetailTv(id: Int) {
        openDetailTv.value = id
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }

    companion object {
        private const val NOT_AVAILABLE = "Data Tidak Tersedia"
    }

}
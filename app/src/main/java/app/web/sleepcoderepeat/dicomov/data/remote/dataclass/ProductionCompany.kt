package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

import java.io.Serializable


data class ProductionCompany(
    val id: Int?,
    val logo_path: String?,
    val name: String?,
    val origin_country: String?
) : Serializable
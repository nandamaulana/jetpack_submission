package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieDetailResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.Constant.Companion.APPEND_TO_RESPONSE
import app.web.sleepcoderepeat.dicomov.utils.getRuntimeFormatted
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl
import com.google.gson.JsonArray
import io.reactivex.disposables.CompositeDisposable

class DetailMovieViewModel @ViewModelInject constructor(
    private val movieTvRepository: MovieTvRepository
) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val movieData = MutableLiveData<MovieDetailResponse>()
    val genreList = MutableLiveData<MutableList<Genre>>()
    val message = MutableLiveData<String>()

    fun getDetail(id: Int, withVideo: Boolean = false) {
        loading.value = true
        movieTvRepository.getDataMovieDetails(
            id,
            if(withVideo) HashMap<String?, String?>().apply {
                put(Constant.APPEND_TO_RESPONSE, "videos")
            }else HashMap(),
            object : MovieTvSource.MovieDetailCallback {
                override fun onSuccess(movieDetailResponse: MovieDetailResponse) {
                    movieDetailResponse.poster_path =
                        movieDetailResponse.poster_path?.toImageUrl()
                    movieData.value = movieDetailResponse
                    movieDetailResponse.genres?.let {
                        genreList.value?.apply {
                            clear()
                            addAll(movieDetailResponse.genres)
                        }
                    }
                    loading.value = false
                }

                override fun onError(msg: String?) {
                    msg?.let {
                        message.value = it
                    }
                    loading.value = false
                }
            }
        ).apply {
            if (!isDisposed) {
                disposable.add(this)
            }
        }
    }


    fun getSpokenLanguages(spokenLanguages: JsonArray?): String {
        var result = ""
        spokenLanguages?.forEach { it ->
            result += it.asJsonObject.get("name").asString + ", "
        }
        return "Language : " + result.dropLast(2)
    }

    fun getRuntimeFormatted(runtime: Int): String {
        return runtime.getRuntimeFormatted()
    }

}
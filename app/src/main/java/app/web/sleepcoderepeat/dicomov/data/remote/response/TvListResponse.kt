package app.web.sleepcoderepeat.dicomov.data.remote.response

import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import com.google.gson.annotations.SerializedName

data class TvListResponse(
    val page: Int,
    @SerializedName("results") val tvListItem: MutableList<TvItem>,
    val total_pages: Int,
    val total_results: Int
)
package app.web.sleepcoderepeat.dicomov.data.remote.response

import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Cast
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Crew

data class CreditResponse(
    val cast: MutableList<Cast>,
    val crew: MutableList<Crew>,
    val id: Int
)
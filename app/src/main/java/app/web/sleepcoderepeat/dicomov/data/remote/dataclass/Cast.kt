package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

data class Cast(
    val cast_id: Int?,
    val character: String?,
    val credit_id: String?,
    val gender: Int?,
    val id: Int?,
    val name: String?,
    val order: Int?,
    var profile_path: String?
)
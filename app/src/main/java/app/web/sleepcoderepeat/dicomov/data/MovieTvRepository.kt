package app.web.sleepcoderepeat.dicomov.data

import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvDataSource
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import io.reactivex.disposables.Disposable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MovieTvRepository @Inject constructor(private val movieTvDataSource: MovieTvDataSource) :
    MovieTvSource {

    override fun getDataDiscoverMovie(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieListCallback
    ): Disposable = movieTvDataSource.getDataDiscoverMovie(map, callback)

    override fun getDataMovieDetails(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.MovieDetailCallback
    ): Disposable = movieTvDataSource.getDataMovieDetails(movieId, map, callback)

    override fun getDataDiscoverTv(
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvListCallback
    ): Disposable = movieTvDataSource.getDataDiscoverTv(map, callback)

    override fun getDataTvDetails(
        tvId: Int,
        map: HashMap<String?, String?>,
        callback: MovieTvSource.TvDetailCallback
    ): Disposable = movieTvDataSource.getDataTvDetails(tvId, map, callback)
}
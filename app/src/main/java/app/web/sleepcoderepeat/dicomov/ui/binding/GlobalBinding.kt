package app.web.sleepcoderepeat.dicomov.ui.binding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.ui.adapter.GenreItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.adapter.MovieItemAdapter
import app.web.sleepcoderepeat.dicomov.ui.adapter.TvItemAdapter
import app.web.sleepcoderepeat.dicomov.utils.load
import com.facebook.shimmer.ShimmerFrameLayout

object GlobalBinding {

    @BindingAdapter("loadImage")
    @JvmStatic
    fun setImage(imageView: ImageView, url: String?) {
        url?.let {
            imageView.load(it)
        }
    }

    @BindingAdapter("genreList")
    @JvmStatic
    fun setGenreList(recyclerView: RecyclerView, dataList: MutableList<Genre>?) {
        dataList?.let {
            (recyclerView.adapter as GenreItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("movieList")
    @JvmStatic
    fun setMovieList(recyclerView: RecyclerView, dataList: MutableList<MovieItem>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as MovieItemAdapter).replaceData(it)
        }
    }

    @BindingAdapter("tvList")
    @JvmStatic
    fun setTvList(recyclerView: RecyclerView, dataList: MutableList<TvItem>?) {
        dataList?.let {
            if (it.size != 0)
                (recyclerView.adapter as TvItemAdapter).replaceData(it)
        }
    }


    @BindingAdapter("isLoading")
    @JvmStatic
    fun setIsLoading(shimmerFrameLayout: ShimmerFrameLayout, isLoading: Boolean) {

        if (isLoading) {
            shimmerFrameLayout.startShimmer()
            return
        }
        shimmerFrameLayout.stopShimmer()
    }
}
package app.web.sleepcoderepeat.dicomov.ui.viewmodel

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.web.sleepcoderepeat.dicomov.data.MovieTvRepository
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.Genre
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvDetailResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.getRuntimeFormatted
import app.web.sleepcoderepeat.dicomov.utils.toImageUrl
import io.reactivex.disposables.CompositeDisposable

class DetailTvViewModel @ViewModelInject constructor(
    private val movieTvRepository: MovieTvRepository
) : ViewModel() {

    private val disposable: CompositeDisposable by lazy { CompositeDisposable() }

    val loading = MutableLiveData<Boolean>()
    val tvData = MutableLiveData<TvDetailResponse>()
    val genreList = MutableLiveData<MutableList<Genre>>()
    val message = MutableLiveData<String>()
    val sumRuntime = MutableLiveData<Int>()

    fun getDetail(id: Int, withVideo: Boolean = false) {
        loading.value = true
            movieTvRepository.getDataTvDetails(
                id,
                if(withVideo) HashMap<String?, String?>().apply {
                    put(Constant.APPEND_TO_RESPONSE, "videos")
                }else HashMap(),
                object : MovieTvSource.TvDetailCallback {
                    override fun onSuccess(tvDetailResponse: TvDetailResponse) {
                        tvDetailResponse.poster_path = tvDetailResponse.poster_path?.toImageUrl()
                        tvData.value = tvDetailResponse
                        sumRuntime.value = tvDetailResponse.episode_run_time?.sum()
                        tvDetailResponse.genres?.let {
                            genreList.value?.apply {
                                clear()
                                addAll(tvDetailResponse.genres)
                            }
                        }
                        loading.value = false
                    }

                    override fun onError(msg: String?) {
                        msg?.let {
                            message.value = msg
                        }
                        loading.value = false
                    }
                }
            ).apply{
                if(!isDisposed){
                    disposable.add(this)
                }
            }
    }

    fun getRuntimeFormatted(runtime: Int): String {
        return runtime.getRuntimeFormatted()
    }

}
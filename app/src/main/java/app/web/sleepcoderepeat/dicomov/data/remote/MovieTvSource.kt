package app.web.sleepcoderepeat.dicomov.data.remote

import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieListResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvListResponse
import io.reactivex.disposables.Disposable


interface MovieTvSource {

    fun getDataDiscoverMovie(
        map: HashMap<String?, String?>,
        callback: MovieListCallback
    ): Disposable

    fun getDataMovieDetails(
        movieId: Int,
        map: HashMap<String?, String?>,
        callback: MovieDetailCallback
    ): Disposable

    fun getDataDiscoverTv(
        map: HashMap<String?, String?>,
        callback: TvListCallback
    ): Disposable

    fun getDataTvDetails(
        tvId: Int,
        map: HashMap<String?, String?>,
        callback: TvDetailCallback
    ): Disposable

    interface MovieListCallback {
        fun onSuccess(movieListResponse: MovieListResponse)
        fun onNotAvailable()
        fun onError(msg: String?)
    }

    interface MovieDetailCallback {
        fun onSuccess(movieDetailResponse: MovieDetailResponse)
        fun onError(msg: String?)
    }

    interface TvListCallback {
        fun onSuccess(tvListResponse: TvListResponse)
        fun onNotAvailable()
        fun onError(msg: String?)
    }

    interface TvDetailCallback {
        fun onSuccess(tvDetailResponse: TvDetailResponse)
        fun onError(msg: String?)
    }


}
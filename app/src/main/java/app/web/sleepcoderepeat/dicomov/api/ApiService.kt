package app.web.sleepcoderepeat.dicomov.api

import app.web.sleepcoderepeat.dicomov.data.remote.response.*
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface ApiService {

    @GET("discover/movie")
    fun getDiscoverMovie(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<MovieListResponse>>

    @GET("movie/{movie_id}")
    fun getMovieDetails(
        @Path("movie_id") id: Int,
        @QueryMap queries: HashMap<String?, String?>?
    ): Observable<Response<MovieDetailResponse>>

    @GET("discover/tv")
    fun getDiscoverTv(@QueryMap queries: HashMap<String?, String?>?): Observable<Response<TvListResponse>>

    @GET("tv/{tv_id}")
    fun getTvDetails(
        @Path("tv_id") id: Int,
        @QueryMap queries: HashMap<String?, String?>?
    ): Observable<Response<TvDetailResponse>>


}
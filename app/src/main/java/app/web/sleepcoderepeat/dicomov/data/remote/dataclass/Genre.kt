package app.web.sleepcoderepeat.dicomov.data.remote.dataclass

import java.io.Serializable

data class Genre(
    val id: Int?,
    val name: String?
) : Serializable
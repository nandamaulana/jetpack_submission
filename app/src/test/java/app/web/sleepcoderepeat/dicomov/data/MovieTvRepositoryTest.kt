package app.web.sleepcoderepeat.dicomov.data

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import app.web.sleepcoderepeat.dicomov.BuildConfig
import app.web.sleepcoderepeat.dicomov.api.ApiService
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvDataSource
import app.web.sleepcoderepeat.dicomov.data.remote.MovieTvSource
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.MovieItem
import app.web.sleepcoderepeat.dicomov.data.remote.dataclass.TvItem
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.MovieListResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvDetailResponse
import app.web.sleepcoderepeat.dicomov.data.remote.response.TvListResponse
import app.web.sleepcoderepeat.dicomov.utils.Constant
import app.web.sleepcoderepeat.dicomov.utils.LiveDataTestUtil
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.verify
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.HiltTestApplication
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import retrofit2.Response
import javax.inject.Inject

@HiltAndroidTest
@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P], application = HiltTestApplication::class)
class MovieTvRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mockitoRule: MockitoRule = MockitoJUnit.rule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    lateinit var apiService: ApiService

    @Mock
    lateinit var dataSource: MovieTvDataSource

    private lateinit var movieTvRepository: MovieTvRepository

    private lateinit var dataMovieResponse: Response<MovieListResponse>
    private lateinit var dataMovieDetailResponse: Response<MovieDetailResponse>

    private lateinit var dataTvResponse: Response<TvListResponse>
    private lateinit var dataTvDetailResponse: Response<TvDetailResponse>

    private val sampleMovieId = 724989
    private val sampleTvId = 71712

    private val mapListQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
        put(Constant.PAGE_KEY, "1")
    }
    private val mapDetailQueries = HashMap<String?, String?>().apply {
        put(Constant.API_KEY, BuildConfig.API_TOKEN)
    }

    @Before
    fun setUp() {
        hiltRule.inject()
        MockitoAnnotations.initMocks(this)
        movieTvRepository = MovieTvRepository(dataSource)
        dataMovieResponse = apiService.getDiscoverMovie(mapListQueries).blockingSingle()
        dataMovieDetailResponse =
            apiService.getMovieDetails(sampleMovieId, mapDetailQueries).blockingSingle()
        dataTvResponse = apiService.getDiscoverTv(mapListQueries).blockingSingle()
        dataTvDetailResponse =
            apiService.getTvDetails(sampleTvId, mapDetailQueries).blockingSingle()
    }

    @Test
    fun getDataDiscoverMovie() {
        `when`(dataSource.getDataDiscoverMovie(eq(mapListQueries), any())).thenAnswer {
            dataMovieResponse.body()?.apply {
                (it.arguments[1] as MovieTvSource.MovieListCallback).onSuccess(this)
            }
            null
        }

        val dataMovieList = MutableLiveData<MutableList<MovieItem>>()
        movieTvRepository.getDataDiscoverMovie(
            mapListQueries,
            object : MovieTvSource.MovieListCallback {
                override fun onSuccess(movieListResponse: MovieListResponse) {
                    val dataList: MutableList<MovieItem> = arrayListOf()
                    dataList.addAll(movieListResponse.movieListItems)
                    dataMovieList.postValue(dataList)
                }

                override fun onNotAvailable() {}

                override fun onError(msg: String?) {}
            })
        val dataMovieListResult = LiveDataTestUtil.getValue(dataMovieList)
        verify(dataSource).getDataDiscoverMovie(eq(mapListQueries), any())
        assertNotNull(dataMovieListResult)
        assertEquals(
            dataMovieResponse.body()?.movieListItems?.size?.toLong(),
            dataMovieListResult.size.toLong()
        )
    }

    @Test
    fun getDataMovieDetails() {
        `when`(
            dataSource.getDataMovieDetails(
                eq(sampleMovieId),
                eq(mapListQueries),
                any()
            )
        ).thenAnswer {
            dataMovieDetailResponse.body()?.apply {
                (it.arguments[2] as MovieTvSource.MovieDetailCallback).onSuccess(this)
            }
            null
        }

        val dataMovieResponse = MutableLiveData<MovieDetailResponse>()
        movieTvRepository.getDataMovieDetails(
            sampleMovieId,
            mapListQueries,
            object : MovieTvSource.MovieDetailCallback {
                override fun onSuccess(movieDetailResponse: MovieDetailResponse) {
                    dataMovieResponse.postValue(movieDetailResponse)
                }

                override fun onError(msg: String?) {}
            })
        val dataMovieResponseResult = LiveDataTestUtil.getValue(dataMovieResponse)
        verify(dataSource).getDataMovieDetails(eq(sampleMovieId),eq(mapListQueries),any())
        assertNotNull(dataMovieResponseResult)
        assertEquals(
            dataMovieDetailResponse.body(),
            dataMovieResponseResult
        )
    }

    @Test
    fun getDataDiscoverTv() {
        `when`(dataSource.getDataDiscoverTv(eq(mapListQueries), any())).thenAnswer {
            dataTvResponse.body()?.apply {
                (it.arguments[1] as MovieTvSource.TvListCallback).onSuccess(this)
            }
            null
        }

        val dataTvList = MutableLiveData<MutableList<TvItem>>()
        movieTvRepository.getDataDiscoverTv(
            mapListQueries,
            object : MovieTvSource.TvListCallback {
                override fun onSuccess(tvListResponse: TvListResponse) {
                    val dataList: MutableList<TvItem> = arrayListOf()
                    dataList.addAll(tvListResponse.tvListItem)
                    dataTvList.postValue(dataList)
                }

                override fun onNotAvailable() {}

                override fun onError(msg: String?) {}
            })
        val dataTvListResult = LiveDataTestUtil.getValue(dataTvList)
        verify(dataSource).getDataDiscoverTv(eq(mapListQueries), any())
        assertNotNull(dataTvListResult)
        assertEquals(
            dataTvResponse.body()?.tvListItem?.size?.toLong(),
            dataTvListResult.size.toLong()
        )
    }

    @Test
    fun getDataTvDetails() {
        `when`(
            dataSource.getDataTvDetails(
                eq(sampleTvId),
                eq(mapListQueries),
                any()
            )
        ).thenAnswer {
            dataTvDetailResponse.body()?.apply {
                (it.arguments[2] as MovieTvSource.TvDetailCallback).onSuccess(this)
            }
            null
        }

        val dataTvResponse = MutableLiveData<TvDetailResponse>()
        movieTvRepository.getDataTvDetails(
            sampleTvId,
            mapListQueries,
            object : MovieTvSource.TvDetailCallback {
                override fun onSuccess(tvDetailResponse: TvDetailResponse) {
                    dataTvResponse.postValue(tvDetailResponse)
                }

                override fun onError(msg: String?) {}
            })
        val dataTvResponseResult = LiveDataTestUtil.getValue(dataTvResponse)
        verify(dataSource).getDataTvDetails(
            eq(sampleTvId),
            eq(mapListQueries),
            any()
        )
        assertNotNull(dataTvResponseResult)
        assertEquals(
            dataTvDetailResponse.body(),
            dataTvResponseResult
        )
    }
}